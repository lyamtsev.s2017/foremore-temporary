# How to run project

## Install dependencies

```bash
yarn install
```

## Run project

```bash
yarn dev
```
